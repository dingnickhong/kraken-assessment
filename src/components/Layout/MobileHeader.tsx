import {
  Anchor,
  Box,
  Burger,
  Button,
  createStyles,
  Divider,
  Drawer,
  Flex,
  Group,
  ScrollArea,
  Text,
} from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import Link from "next/link";
import { useRouter } from "next/router";
import { stylesConfig } from "src/styles/mantine-common";

const useStyles = createStyles((theme) => ({
  ...stylesConfig(theme),
  burgerContainer: {
    padding: 2,
    backgroundColor: "black",
    borderRadius: theme.radius.xl,
    justifyContent: "center",
    alignItems: "cen",
  },
  link: {
    display: "flex",
    alignItems: "center",
    height: "100%",
    padding: theme.spacing.md,
    textDecoration: "none",
    color: theme.colorScheme === "dark" ? theme.white : theme.black,
    fontWeight: 500,
    fontSize: theme.fontSizes.sm,

    [theme.fn.smallerThan("sm")]: {
      height: 42,
      display: "flex",
      alignItems: "center",
      width: "100%",
    },

    ...theme.fn.hover({
      backgroundColor:
        theme.colorScheme === "dark"
          ? theme.colors.dark[6]
          : theme.colors.gray[0],
    }),
  },
  signIn: {
    border: `1px solid ${theme.colors.gray[2]}`,
  },
}));

const mobileLinks = [
  { label: "Home", href: "#" },
  { label: "Link 1", href: "#" },
  { label: "Link 2", href: "#" },
  { label: "Link 3", href: "#" },
  { label: "Link 4", href: "#" },
  { label: "Link 5", href: "#" },
];

const MobileHeader = () => {
  const router = useRouter();

  const [navOpened, handlers] = useDisclosure(false);
  const { classes, theme } = useStyles();

  return (
    <>
      <Group className={classes.hiddenDesktop} position="apart">
        <Group
          w="100%"
          spacing="xs"
          position="apart"
          sx={{ flexWrap: "nowrap" }}
        >
          <Group
            spacing={0}
            align="start"
            sx={{ cursor: "pointer" }}
            onClick={() => router.push("/")}
          >
            <Text weight={900} size="xl" sx={{ lineHeight: 1.2 }}>
              BRAND
            </Text>
            <Text weight={900} size={8}>
              TM
            </Text>
          </Group>
          <Flex className={classes.burgerContainer}>
            <Burger
              opened={navOpened}
              onClick={handlers.toggle}
              size="sm"
              color="white"
              styles={{
                root: {
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                },
              }}
            />
          </Flex>
        </Group>
      </Group>
      <Drawer
        opened={navOpened}
        onClose={handlers.close}
        size="100%"
        padding="md"
        title={
          <Group
            spacing={0}
            align="start"
            sx={{ cursor: "pointer" }}
            onClick={() => router.push("/")}
          >
            <Text weight={900} size="xl" sx={{ lineHeight: 1.2 }}>
              BRAND
            </Text>
            <Text weight={900} size={8}>
              TM
            </Text>
          </Group>
        }
        styles={{ header: { marginBottom: "0", borderBottom: 0 } }}
        className={classes.hiddenDesktop}
        zIndex={1000000}
      >
        <ScrollArea sx={{ height: "calc(100vh - 60px)" }} mx="-md">
          <Divider
            my="sm"
            color={theme.colorScheme === "dark" ? "dark.5" : "gray.1"}
          />

          {mobileLinks.map((link, id) => (
            <Link href="/" passHref key={id}>
              <Anchor className={classes.link} onClick={handlers.close}>
                {link.label}
              </Anchor>
            </Link>
          ))}
          <Box px="md" mt="md">
            <Button
              onClick={() => {
                router.push("/auth/sign-in");
                handlers.close();
              }}
              color="dark.9"
              fullWidth
            >
              Sign In
            </Button>
          </Box>
        </ScrollArea>
      </Drawer>
    </>
  );
};

export default MobileHeader;
