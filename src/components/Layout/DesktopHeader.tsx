import {
  Anchor,
  Box,
  Button,
  Center,
  createStyles,
  Flex,
  Group,
  Text,
} from "@mantine/core";
import Link from "next/link";
import { useRouter } from "next/router";
import { stylesConfig } from "src/styles/mantine-common";
import { useHydrated } from "src/utils/hooks/useHydrated";

const useStyles = createStyles((theme) => ({
  ...stylesConfig(theme),
  link: {
    display: "flex",
    alignItems: "center",
    gap: theme.spacing.xs,
    cursor: "pointer",
    height: "100%",
    fontWeight: 500,
    fontSize: theme.fontSizes.md,
    color: "black",

    [theme.fn.smallerThan("sm")]: {
      height: 42,
      display: "flex",
      alignItems: "center",
      width: "100%",
    },
  },
}));

const desktopLinks = [
  { label: "Link 1", href: "#" },
  { label: "Link 2", href: "#" },
  { label: "Link 3", href: "#" },
  { label: "Link 4", href: "#" },
  { label: "Link 5", href: "#" },
];

export const DesktopHeader = () => {
  const router = useRouter();
  const { classes, theme } = useStyles();

  const hydrated = useHydrated();

  return (
    <Flex justify="space-between" className={classes.hiddenMobile}>
      <Group>
        <Box>
          <Link href="/">
            <Group
              spacing={0}
              align="start"
              sx={{ cursor: "pointer" }}
              onClick={() => router.push("/")}
            >
              <Text weight={900} size={24} sx={{ lineHeight: 1.2 }}>
                BRAND
              </Text>
              <Text weight={900} size={8}>
                TM
              </Text>
            </Group>
          </Link>
        </Box>
      </Group>
      <Group spacing={24}>
        {hydrated &&
          desktopLinks.map((link, id) => (
            <Link href="/" passHref key={id}>
              <Anchor component="p" className={classes.link}>
                {link.label}
              </Anchor>
            </Link>
          ))}
      </Group>
      <Group>
        <Center>
          <Group spacing={6}>
            <Button
              radius="xl"
              color="dark.9"
              onClick={() => {
                router.push("/auth/sign-in");
              }}
            >
              Sign In
            </Button>
          </Group>
        </Center>
      </Group>
    </Flex>
  );
};
