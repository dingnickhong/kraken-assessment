import {
  Anchor,
  Box,
  Container,
  createStyles,
  Flex,
  Footer,
  Group,
  Stack,
  Text,
  TextInput,
} from "@mantine/core";
import { useRouter } from "next/router";
import { FaPaperPlane } from "react-icons/fa";
import { useHydrated } from "src/utils/hooks/useHydrated";

const useStyles = createStyles((theme) => ({
  footer: {
    padding: theme.spacing.md,
    paddingTop: theme.spacing.lg * 2,
    paddingBottom: theme.spacing.lg * 2,
    backgroundColor: "#000",
    flexGrow: 0,
    flexShrink: 0,
    zIndex: 1,

    [theme.fn.smallerThan("sm")]: {
      paddingLeft: theme.spacing.xl,
      paddingRight: theme.spacing.xl,
    },
  },
  container: {
    display: "flex",
    width: "100%",
    gap: theme.spacing.xl,

    [theme.fn.smallerThan("sm")]: {
      gap: 0,
      flexDirection: "column",
    },
  },
  desktopLogo: {
    display: "flex",
    maxWidth: "150px",
    flex: 1,
    [theme.fn.smallerThan("md")]: {
      display: "none",
    },
  },
  mobileLogo: {
    display: "none",
    [theme.fn.smallerThan("md")]: {
      display: "flex",
    },
  },
  section1: {
    flex: 2,
    width: "100%",
    flexDirection: "column",
    gap: theme.spacing.md,
    [theme.fn.smallerThan("sm")]: {
      display: "flex",
      alignItems: "center",
      width: "100%",
    },
  },
  linkCategories: {
    flexWrap: "nowrap",
  },
  linkCategory: {
    maxWidth: "150px",
    width: "100%",
    alignSelf: "start",
    [theme.fn.smallerThan("md")]: {},
    [theme.fn.smallerThan("sm")]: {
      flex: 1,
      alignItems: "center",
    },
  },
  linkCategoryTitle: {
    [theme.fn.smallerThan("sm")]: { textAlign: "center" },
  },
  linkContainer: {
    [theme.fn.smallerThan("sm")]: { textAlign: "center" },
  },
  link: {
    gap: theme.spacing.xs,
    cursor: "pointer",
    height: "100%",
    fontWeight: 500,
    fontSize: theme.fontSizes.xs,
    color: theme.colors.gray[5],

    [theme.fn.smallerThan("sm")]: {
      textAlign: "center",
    },
  },
  section2: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    gap: theme.spacing.md,
    flex: 1,
    [theme.fn.smallerThan("sm")]: {
      alignItems: "center",
      width: "100%",
      paddingTop: theme.spacing.md,
    },
  },
  newsletterDesc: {
    fontSize: theme.fontSizes.xs,
    [theme.fn.smallerThan("sm")]: {
      textAlign: "center",
    },
  },
}));

const linkCategories = [
  {
    label: "First column",
    links: [
      { label: "First page", href: "#" },
      { label: "Second page", href: "#" },
      { label: "Third", href: "#" },
      { label: "Fourth", href: "#" },
    ],
  },
  {
    label: "Second",
    links: [
      { label: "Fifth page", href: "#" },
      { label: "Sixth page", href: "#" },
      { label: "Eighth", href: "#" },
    ],
  },
  {
    label: "Third",
    links: [
      { label: "Fifth page", href: "#" },
      { label: "Sixth page", href: "#" },
      { label: "Eighth page", href: "#" },
    ],
  },
];

export function AppFooter() {
  const { theme, classes } = useStyles();
  const router = useRouter();

  // const mobile = useMediaQuery("(max-width: 480px)");

  const hydrated = useHydrated();

  return (
    <>
      <Footer height="auto" className={classes.footer} withBorder={false}>
        <Container size="lg" px={0} className={classes.container}>
          <Group
            spacing={0}
            align="start"
            sx={{ cursor: "pointer" }}
            className={classes.desktopLogo}
            onClick={() => router.push("/")}
          >
            <Text weight={900} size="xl" color="white" sx={{ lineHeight: 1.2 }}>
              BRAND
            </Text>
            <Text weight={900} size={8} color="white">
              TM
            </Text>
          </Group>
          <Flex className={classes.section1}>
            <Group
              spacing={0}
              align="start"
              sx={{ cursor: "pointer" }}
              className={classes.mobileLogo}
              onClick={() => router.push("/")}
            >
              <Text
                weight={900}
                size="xl"
                color="white"
                sx={{ lineHeight: 1.2 }}
              >
                BRAND
              </Text>
              <Text weight={900} size={8} color="white">
                TM
              </Text>
            </Group>
            <Group w="100%" className={classes.linkCategories}>
              {hydrated &&
                linkCategories.map((category) => (
                  <Stack
                    key={category.label}
                    className={classes.linkCategory}
                    spacing={8}
                  >
                    <Text
                      className={classes.linkCategoryTitle}
                      size="sm"
                      weight="bold"
                      color="white"
                    >
                      {category.label}
                    </Text>
                    <Stack spacing={0}>
                      {category.links.map((link, id) => (
                        <Box className={classes.linkContainer} key={id}>
                          <Anchor className={classes.link}>{link.label}</Anchor>
                        </Box>
                      ))}
                    </Stack>
                  </Stack>
                ))}
            </Group>
          </Flex>
          <Flex className={classes.section2}>
            <Text
              className={classes.linkCategoryTitle}
              size="sm"
              weight="bold"
              color="white"
            >
              Subscribe
            </Text>
            <TextInput
              radius="xl"
              width="100%"
              styles={{
                root: { width: "100%" },
                input: {
                  backgroundColor: "#262626",
                  border: "none",
                  paddingLeft: theme.spacing.lg,
                  paddingRight: theme.spacing.lg,
                  color: "white",
                },
              }}
              placeholder="Enter email"
              rightSection={
                <Box pr="md">
                  <FaPaperPlane color="white" size={10} />
                </Box>
              }
            />
            <Text className={classes.newsletterDesc} color="dimmed">
              Join our newsletter to stay up to date on features and releases
            </Text>
          </Flex>
        </Container>
      </Footer>
    </>
  );
}
