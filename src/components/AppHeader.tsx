import { Container, createStyles, Header } from "@mantine/core";
import { DesktopHeader } from "./Layout/DesktopHeader";

import MobileHeader from "./Layout/MobileHeader";

const useStyles = createStyles((theme) => ({
  header: {
    flexGrow: 0,
    flexShrink: 0,
    zIndex: 1,
  },
  container: {},
}));

export function AppHeader() {
  const { classes } = useStyles();

  return (
    <Header height="auto" p="md" withBorder={false} className={classes.header}>
      <Container size="xl" px={0} className={classes.container}>
        <MobileHeader />
        <DesktopHeader />
      </Container>
    </Header>
  );
}
