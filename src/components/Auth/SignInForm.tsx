import {
  Anchor,
  Box,
  Button,
  Checkbox,
  Container,
  Divider,
  Flex,
  Group,
  PasswordInput,
  Text,
  TextInput,
  Title,
} from "@mantine/core";
import { useForm, zodResolver } from "@mantine/form";
import { useTimeout } from "@mantine/hooks";
import { showNotification } from "@mantine/notifications";
import Link from "next/link";
import { useRouter } from "next/router";
import { useCallback, useState } from "react";
import * as z from "zod";

import { FacebookButton, GoogleButton } from "./SocialButtons/SocialButtons";

type LoginInputs = {
  email: string;
  password: string;
  remember_me: boolean;
};

const pageContent: { [index: string]: any } = {
  login: {
    title: "Login",
    description: "Sign in to your account",
    email: "Email",
    password: "Password",
    forgot: "Forgot Password",
    stay_signed: "Stay signed in for a week",
    button: "Continue",
  },
  loginValidation: {
    invalid_email: "Invalid email address",
    name_required: "Please enter your full name",
    email_required: "Please enter your email address",
    pass1_required: "Please enter your password",
    pass2_required: "Please confirm your password",
  },
};

export const SignInForm = () => {
  const router = useRouter();
  const [loading, setLoading] = useState(false);

  const { loginValidation } = pageContent;

  const loginSchema = z
    .object({
      email: z
        .string()
        .email(`${loginValidation.invalid_email}`)
        .min(1, "Email is required"),
      password: z.string().min(1, loginValidation.pass1_required),
      remember_me: z.boolean().default(false),
    })
    .required();

  const form = useForm<LoginInputs>({
    validate: zodResolver(loginSchema),
  });

  const { start, clear } = useTimeout(() => {
    setLoading(false);
    showNotification({
      message:
        "Successful! Your inputs are validated however this Sign Up Form is for demo only!",
      color: "green",
    });
  }, 1000);

  const onSubmit = useCallback(async (data: LoginInputs) => {
    setLoading(true);
    start();
  }, []);

  return (
    <Container size={620} my={40} w="100%">
      <Title align="center" sx={(theme) => ({})}>
        Welcome back!
      </Title>
      <Text color="dimmed" size="sm" align="center" mt={5}>
        Do not have an account yet?{" "}
        <Link href="/auth/sign-up">Create account</Link>
      </Text>

      <Box pt="md">
        <form onSubmit={form.onSubmit((values) => onSubmit(values))}>
          <TextInput
            label="Email"
            placeholder="you@gmail.com"
            {...form.getInputProps("email")}
            autoCapitalize="off"
            withAsterisk
            type="email"
            mt="md"
          />
          <PasswordInput
            label="Password"
            placeholder="Your password"
            {...form.getInputProps("password")}
            autoCapitalize="off"
            withAsterisk
            mt="md"
          />
          <Group position="apart" mt="lg">
            <Checkbox
              label="Remember me"
              sx={{ lineHeight: 1 }}
              {...form.getInputProps("remember_me")}
            />
            <Anchor<"a">
              onClick={(event) => event.preventDefault()}
              href="/"
              size="sm"
            >
              Forgot password?
            </Anchor>
          </Group>
          <Button fullWidth mt="xl" type="submit" color="dark.9">
            <span color="white">{loading ? "Signing in..." : "Sign in"}</span>
          </Button>
        </form>
      </Box>
      <Divider
        my="lg"
        label={<Text size="xs">Or continue with</Text>}
        labelPosition="center"
      />
      <Flex gap="md">
        <GoogleButton
          w="100%"
          onClick={() => {
            showNotification({
              message: "Sign In from Google! Or not...",
              color: "green",
            });
          }}
        >
          Google
        </GoogleButton>
        <FacebookButton
          w="100%"
          onClick={() => {
            showNotification({
              message: "Sign In from Facebook! Or not...",
              color: "green",
            });
          }}
        >
          Facebook
        </FacebookButton>
      </Flex>
    </Container>
  );
};
