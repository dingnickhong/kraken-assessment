import { createStyles, Flex, Image, List, Stack, Text } from "@mantine/core";
import { stylesConfig } from "src/styles/mantine-common";
import DefaultContainer from "../General/DefaultContainer";

const useStyles = createStyles((theme) => ({
  ...stylesConfig(theme),
  contentContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    gap: theme.spacing.md,
    [theme.fn.largerThan("lg")]: {
      flexDirection: "row",
      justifyContent: "space-between",
      paddingTop: theme.spacing.xl,
      paddingBottom: theme.spacing.xl,
    },
  },
  content: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    gap: theme.spacing.md,
    [theme.fn.largerThan("md")]: {
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "start",
      paddingTop: theme.spacing.xl,
      paddingBottom: theme.spacing.xl,
    },
    [theme.fn.largerThan("lg")]: {
      flexDirection: "column",
      justifyContent: "unset",
      alignItems: "start",
      paddingTop: 0,
      paddingBottom: 0,
      width: 420,
    },
  },

  contentHeader: {
    fontSize: theme.fontSizes.sm,
    fontWeight: "bold",
    color: theme.white,
    textAlign: "center",
    [theme.fn.largerThan("sm")]: {
      fontSize: theme.fontSizes.xl,
    },
    [theme.fn.largerThan("md")]: {
      textAlign: "start",
      width: "35%",
    },
    [theme.fn.largerThan("lg")]: {
      width: "100%",
    },
  },
  contentDetails: {
    [theme.fn.largerThan("md")]: {
      width: "50%",
    },
    [theme.fn.largerThan("lg")]: {
      width: "100%",
    },
  },
  contentDesc: {
    textAlign: "center",
    fontSize: theme.fontSizes.xs,
    color: theme.colors.gray[4],
    [theme.fn.largerThan("sm")]: {
      fontSize: theme.fontSizes.md,
    },
    [theme.fn.largerThan("md")]: {
      textAlign: "start",
    },
  },
  contentListItem: {
    color: theme.colors.gray[4],
    fontSize: theme.fontSizes.xs,
    [theme.fn.largerThan("sm")]: {
      fontSize: theme.fontSizes.md,
    },
  },
  listItemDecorator: {
    lineHeight: 0.9,
    [theme.fn.largerThan("sm")]: {
      lineHeight: 1.2,
    },
  },
  contentImage: {
    maxHeight: 520,
    aspectRatio: "2/1",
    [theme.fn.largerThan("md")]: {
      maxHeight: 480,
    },
    [theme.fn.largerThan("lg")]: {
      maxHeight: 350,
      maxWidth: 450,
      aspectRatio: "unset",
    },
  },
  image: {
    maxHeight: 520,
    [theme.fn.largerThan("md")]: {
      maxHeight: 480,
    },
    [theme.fn.largerThan("lg")]: {
      maxHeight: 350,
      maxWidth: 450,
    },
  },
}));

const contents = [
  { label: "Showcase and embed your work with" },
  { label: "Publish across social channels in a click" },
  { label: "Sell your videos worldwide" },
  { label: "Embed vour work with" },
];

const Content = () => {
  const { classes, theme } = useStyles();
  return (
    <section
      className={classes.section}
      style={{
        backgroundColor: theme.colors.dark[9],
      }}
    >
      <DefaultContainer>
        <Flex className={classes.contentContainer}>
          <Image
            src="https://picsum.photos/520/520"
            radius="md"
            classNames={{
              root: classes.contentImage,
              image: classes.contentImage,
            }}
          />
          <Flex className={classes.content}>
            <Text className={classes.contentHeader}>
              Long headline on two lines to turn your visit into users and
              achieve more
            </Text>
            <Stack className={classes.contentDetails}>
              <Text component="p" className={classes.contentDesc}>
                Separated they live in Bookmarks right at the coast of the
                famous Semantics, large language ocean
              </Text>
              <List>
                {contents.map((content) => (
                  <List.Item
                    key={content.label}
                    icon={
                      <Text color="white" className={classes.listItemDecorator}>
                        •
                      </Text>
                    }
                  >
                    <Text className={classes.contentListItem}>
                      {content.label}
                    </Text>
                  </List.Item>
                ))}
              </List>
            </Stack>
          </Flex>
        </Flex>
      </DefaultContainer>
    </section>
  );
};

export default Content;
