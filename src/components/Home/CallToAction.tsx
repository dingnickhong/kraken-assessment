import {
  Box,
  Button,
  Container,
  createStyles,
  Flex,
  Image,
  Overlay,
  Stack,
  Text,
} from "@mantine/core";

const useStyles = createStyles((theme) => ({
  section: {
    paddingTop: theme.spacing.md,
    paddingBottom: theme.spacing.md,
    height: 500,
    position: "relative",
  },
  header: {
    fontWeight: 800,
    fontSize: theme.fontSizes.xl,
    textAlign: "center",
    color: theme.white,
    [theme.fn.largerThan("sm")]: {
      fontSize: 32,
    },
  },
  tagline: {
    color: theme.white,
    fontSize: theme.fontSizes.md,
  },
  callToActionContainer: {
    height: "100%",
    position: "relative",
    justifyContent: "center",
    alignItems: "center",
  },
  actionContainer: {
    flexDirection: "column",
    alignItems: "center",
    gap: theme.spacing.md,
    [theme.fn.largerThan("sm")]: {},
  },
  imageContainer: { position: "absolute", zIndex: -1, width: "100%" },
  image: { height: 500, width: "100%" },
}));

const CallToAction = () => {
  const { classes, theme } = useStyles();
  return (
    <section className={classes.section}>
      <Overlay opacity={0.3} color="#000" zIndex={0} />
      <Flex className={classes.callToActionContainer}>
        <Box className={classes.imageContainer}>
          <Image
            src="https://picsum.photos/1920/1080/"
            classNames={{ image: classes.image }}
            height={500}
            mah={580}
          />
        </Box>
        <Container size="sm" px={60}>
          <Stack>
            <Box>
              <Text component="p" ta="center" className={classes.tagline}>
                Tagline
              </Text>
              <Text component="h1" className={classes.header}>
                Long headline to turn your visitors into users
              </Text>
            </Box>
            <Flex className={classes.actionContainer}>
              <Button
                w="min-content"
                radius="xl"
                variant="white"
                color={"dark.9"}
              >
                Action
              </Button>
            </Flex>
          </Stack>
        </Container>
      </Flex>
    </section>
  );
};

export default CallToAction;
