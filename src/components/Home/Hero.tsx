import { Button, createStyles, Flex, Image, Stack, Text } from "@mantine/core";
import { stylesConfig } from "src/styles/mantine-common";
import DefaultContainer from "../General/DefaultContainer";

const useStyles = createStyles((theme) => ({
  ...stylesConfig(theme),
  heroContainer: {
    flexDirection: "column",
    gap: theme.spacing.md,
    [theme.fn.largerThan("md")]: {
      // paddingBottom: theme.spacing.xl,
    },
    [theme.fn.largerThan("lg")]: {
      flexDirection: "row",
      width: "100%",
      justifyContent: "space-between",
      maxWidth: "none",
      paddingTop: theme.spacing.xl * 2,
      paddingBottom: theme.spacing.xl * 2,
    },
  },
  heroDetailsContainer: {
    [theme.fn.largerThan("md")]: {
      alignSelf: "center",
      maxWidth: 500,
      paddingBottom: theme.spacing.xl,
    },
  },
  header: {
    fontWeight: 800,
    fontSize: 32,
    lineHeight: 1.2,
    textAlign: "center",
    [theme.fn.largerThan("sm")]: {
      fontSize: 40,
    },
    [theme.fn.largerThan("md")]: {
      fontSize: 48,
    },
    [theme.fn.largerThan("lg")]: {
      textAlign: "start",
    },
  },
  subheader: {
    fontSize: theme.fontSizes.md,
    [theme.fn.largerThan("sm")]: {
      fontSize: theme.fontSizes.lg,
    },
    [theme.fn.largerThan("lg")]: {
      textAlign: "start",
    },
  },
  actionContainer: {
    flexDirection: "column",
    alignItems: "center",
    gap: theme.spacing.md,
    [theme.fn.largerThan("md")]: {
      flexDirection: "row",
      justifyContent: "center",
    },
    [theme.fn.largerThan("lg")]: {
      justifyContent: "start",
    },
  },
  actionDesc: {
    fontSize: theme.fontSizes.xs,
    fontWeight: "bold",
    textAlign: "center",
    [theme.fn.largerThan("sm")]: {
      fontSize: theme.fontSizes.sm,
    },
    [theme.fn.largerThan("lg")]: {
      textAlign: "start",
      width: 200,
    },
  },
  heroImage: {
    maxHeight: 520,
    [theme.fn.largerThan("md")]: {
      maxHeight: 480,
    },
    [theme.fn.largerThan("lg")]: {
      maxHeight: 380,
      maxWidth: 480,
    },
  },
  image: {
    maxHeight: 520,
    [theme.fn.largerThan("md")]: {
      maxHeight: 480,
    },
    [theme.fn.largerThan("lg")]: {
      maxHeight: 380,
      maxWidth: 480,
    },
  },
}));

const Hero = () => {
  const { classes, theme } = useStyles();
  return (
    <section className={classes.section}>
      <DefaultContainer>
        <Flex className={classes.heroContainer}>
          <Stack className={classes.heroDetailsContainer}>
            <Text component="h1" className={classes.header}>
              Medium length display headline
            </Text>
            <Text component="p" ta="center" className={classes.subheader}>
              Separated the live in Bookmarks right at the coas of the famous
              Semantics, arge anguage
            </Text>
            <Flex className={classes.actionContainer}>
              <Button w="min-content" radius="xl" color="dark.9">
                Action
              </Button>
              <Text component="p" className={classes.actionDesc}>
                5,000 people like you have purchased this product.
              </Text>
            </Flex>
          </Stack>
          <Image
            src="https://picsum.photos/520/520"
            radius="md"
            className={classes.heroImage}
            classNames={{ image: classes.image }}
          />
        </Flex>
      </DefaultContainer>
    </section>
  );
};

export default Hero;
