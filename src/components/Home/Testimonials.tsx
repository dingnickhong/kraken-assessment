import { Carousel } from "@mantine/carousel";
import {
  Avatar,
  Box,
  Container,
  createStyles,
  Flex,
  Group,
  Stack,
  Text,
} from "@mantine/core";
import { useMediaQuery } from "@mantine/hooks";
import { useMemo } from "react";
import { FaFacebook, FaTwitter } from "react-icons/fa";

const useStyles = createStyles((theme) => ({
  section: {
    paddingTop: theme.spacing.md,
    paddingBottom: theme.spacing.xl * 2,
    [theme.fn.largerThan("sm")]: {
      paddingTop: theme.spacing.xl * 2,
      paddingBottom: theme.spacing.xl * 3,
    },
  },
  slide: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "center",
    gap: theme.spacing.md,
    backgroundColor: "#F3F3F3",
    height: 380,
    borderRadius: theme.radius.md,
    padding: theme.spacing.md,
    paddingTop: theme.spacing.lg,
    paddingBottom: theme.spacing.lg,
    [theme.fn.largerThan("sm")]: {
      height: 300,
    },
  },
  indicators: { bottom: -36 },
  indicator: {
    height: 16,
    width: 16,
    backgroundColor: "black",
    //   backgroundColor: theme.colors.gray[4],
    opacity: 0.1,
  },
  testimonialLabel: {
    textAlign: "center",
    fontSize: theme.fontSizes.sm,
    [theme.fn.largerThan("sm")]: {
      fontSize: theme.fontSizes.md,
    },
  },
  testimonialDetailConatainer: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    gap: theme.spacing.md,
    [theme.fn.largerThan("md")]: {
      width: "100%",
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "end",
    },
  },
  testimonialUser: {
    alignItems: "center",
    [theme.fn.largerThan("md")]: {
      alignItems: "start",
    },
  },
  testimonialAvatar: {},
  testimonialUserDetails: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    [theme.fn.largerThan("md")]: {
      alignItems: "start",
    },
  },
  testimonialUsername: {
    fontSize: theme.fontSizes.xs,
    fontWeight: "bold",
  },
  testimonialUserdesc: {
    fontSize: theme.fontSizes.xs,
    color: theme.colors.gray[8],
  },
}));

const testimonials = [
  {
    label:
      "A testimonial describing what the person thinks about this service product or startup in general.",
  },
  {
    label:
      "A testimonial describing what the person thinks about this service product or startup in general.",
  },
  {
    label:
      "A testimonial describing what the person thinks about this service product or startup in general.",
  },
];

const Testimonials = () => {
  const { classes, theme } = useStyles();

  const smallerMobile = useMediaQuery("(max-width: 479px)");
  const mobile = useMediaQuery("(max-width: 767px)");

  const slideSize = useMemo(() => {
    if (smallerMobile) return "100%";
    if (mobile) return "50%";
    return "33.3333%";
  }, [smallerMobile, mobile]);

  const slidesToScroll = useMemo(() => {
    if (smallerMobile) return 1;
    if (mobile) return 2;
    return 3;
  }, [smallerMobile, mobile]);

  return (
    <section className={classes.section}>
      <Container size="lg" w="100%">
        <Carousel
          w="100%"
          height={smallerMobile ? 380 : 300}
          slideGap="md"
          slideSize={slideSize}
          loop
          align="start"
          withIndicators
          withControls={false}
          slidesToScroll={slidesToScroll}
          classNames={{
            indicators: classes.indicators,
            indicator: classes.indicator,
          }}
        >
          {testimonials.map((testimonial, id) => (
            <Carousel.Slide key={`testimonial ${id}`}>
              <Stack className={classes.slide}>
                <Text className={classes.testimonialLabel}>
                  &quot;{testimonial.label}&quot;
                </Text>
                <Flex className={classes.testimonialDetailConatainer}>
                  <Stack className={classes.testimonialUser}>
                    <Avatar
                      className={classes.testimonialAvatar}
                      src="https://picsum.photos/80/80"
                      radius="xl"
                      size="lg"
                    />
                    <Box className={classes.testimonialUserDetails}>
                      <Text className={classes.testimonialUsername}>
                        Name Surname
                      </Text>
                      <Text className={classes.testimonialUserdesc}>
                        Description
                      </Text>
                    </Box>
                  </Stack>
                  <Group>
                    <FaFacebook />
                    <FaTwitter />
                  </Group>
                </Flex>
              </Stack>
            </Carousel.Slide>
          ))}
        </Carousel>
      </Container>
    </section>
  );
};

export default Testimonials;
