import { Box, createStyles, Flex, Group, Text } from "@mantine/core";
import { stylesConfig } from "src/styles/mantine-common";
import DefaultContainer from "../General/DefaultContainer";

const useStyles = createStyles((theme) => ({
  ...stylesConfig(theme),
  featureContainer: {
    gap: 32,
    flexDirection: "column",
    [theme.fn.largerThan("md")]: {
      flexDirection: "row",
      paddingTop: theme.spacing.md,
      paddingBottom: theme.spacing.md,
    },
  },
  feature: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    gap: theme.spacing.xs,
    [theme.fn.largerThan("md")]: {
      alignItems: "start",
    },
  },
  box: {
    height: 30,
    width: 30,
    borderRadius: theme.radius.xl,
    backgroundColor: theme.colors.dark[9],
    [theme.fn.largerThan("sm")]: {
      height: 35,
      width: 35,
    },
  },
  featureLabel: {
    fontSize: theme.fontSizes.md,
    fontWeight: "bold",
    [theme.fn.largerThan("sm")]: {
      fontSize: theme.fontSizes.lg,
    },
  },
  featureDesc: {
    fontSize: theme.fontSizes.sm,
    textAlign: "center",
    [theme.fn.largerThan("sm")]: {
      fontSize: theme.fontSizes.md,
    },
    [theme.fn.largerThan("md")]: {
      textAlign: "start",
    },
  },
}));

const features = [
  { label: "Feature one" },
  { label: "Feature two" },
  { label: "Feature three" },
];

const Features = () => {
  const { classes, theme } = useStyles();
  return (
    <section className={classes.section} style={{ backgroundColor: "#F3F3F3" }}>
      <DefaultContainer>
        <Flex className={classes.featureContainer}>
          {features.map((feature) => (
            <Flex className={classes.feature} key={feature.label}>
              <Group>
                <Box className={classes.box} />
                <Text className={classes.featureLabel}>{feature.label}</Text>
              </Group>
              <Text component="p" className={classes.featureDesc}>
                All base Ul elements are made using Nested sumbos and shared
                stles that are logicall connected win one anoter.
              </Text>
            </Flex>
          ))}
        </Flex>
      </DefaultContainer>
    </section>
  );
};

export default Features;
