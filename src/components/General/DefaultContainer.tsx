import { Container } from "@mantine/core";
import { ReactNode } from "react";

type IContainerProps = {
  children: ReactNode;
  className?: string;
};

const DefaultContainer = (props: IContainerProps) => {
  return (
    <Container size="lg" w="100%" {...props}>
      {props.children}
    </Container>
  );
};

export default DefaultContainer;
