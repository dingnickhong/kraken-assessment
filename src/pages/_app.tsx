import { MantineProvider } from "@mantine/core";
import { NotificationsProvider } from "@mantine/notifications";
import type { AppProps } from "next/app";
import "src/styles/globals.css";
import mantineTheme from "src/styles/theme";

export default function App({ Component, pageProps }: AppProps) {
  return (
    <MantineProvider
      withGlobalStyles
      withNormalizeCSS
      theme={{
        ...mantineTheme,
        breakpoints: {
          xs: 360,
          sm: 480,
          md: 768,
          lg: 1024,
          xl: 1440,
        },
      }}
    >
      <NotificationsProvider position="bottom-center">
        <Component {...pageProps} />
      </NotificationsProvider>
    </MantineProvider>
  );
}
