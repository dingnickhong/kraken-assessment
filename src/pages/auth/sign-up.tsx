// chakra imports
import { Box, createStyles } from "@mantine/core";
import type { NextPage } from "next";
import { App } from "src/layout/App";
import { SignUpForm } from "src/src/components/Auth/SignUpForm";

const useStyles = createStyles((theme) => ({
  container: {
    width: "100%",
    backgroundColor: "#f3f3f3",
  },
}));

const SignIn: NextPage = () => {
  const { theme, classes } = useStyles();

  return (
    <App>
      <Box className={classes.container}>
        <SignUpForm />
      </Box>
    </App>
  );
};

export default SignIn;
