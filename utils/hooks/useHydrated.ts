import { useEffect, useState } from "react";

export const useHydrated = () => {
  const [hydrated, setHydrated] = useState<boolean>(false);

  useEffect(() => {
    setHydrated(true);
  }, []);

  return hydrated;
};
