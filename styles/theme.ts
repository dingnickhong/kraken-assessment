import { MantineThemeOverride } from "@mantine/core";

const mantineTheme: MantineThemeOverride = {
  colorScheme: "light",
  components: {},
  globalStyles: (theme) => ({}),
};

export default mantineTheme;
