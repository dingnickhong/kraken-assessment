import { MantineTheme } from "@mantine/core";

export const stylesConfig = (theme: MantineTheme) => ({
  section: {
    paddingTop: theme.spacing.md,
    paddingBottom: theme.spacing.md,
    [theme.fn.largerThan("sm")]: {
      paddingTop: theme.spacing.lg,
      paddingBottom: theme.spacing.lg,
    },
    [theme.fn.largerThan("md")]: {
      paddingTop: theme.spacing.xl,
      paddingBottom: theme.spacing.xl,
    },
  },
  px: {
    paddingLeft: theme.spacing.md,
    paddingRight: theme.spacing.md,
    ["@media (min-width: 1172px)"]: {
      paddingLeft: 0,
      paddingRight: 0,
    },
  },
  hiddenMobile: {
    [theme.fn.smallerThan("md")]: {
      display: "none",
    },
  },
  hiddenDesktop: {
    [theme.fn.largerThan("md")]: {
      display: "none",
    },
  },
});
