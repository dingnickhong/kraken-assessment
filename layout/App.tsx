import { Flex } from "@mantine/core";
import { ReactNode } from "react";
import { AppFooter } from "src/src/components/AppFooter";
import { AppHeader } from "src/src/components/AppHeader";

type IAppProps = {
  children: ReactNode;
};

const App = (props: IAppProps) => {
  return (
    <Flex
      direction="column"
      mih="100vh"
      sx={{
        boxSizing: "border-box",
      }}
    >
      <AppHeader />
      <Flex sx={{ flexGrow: 1 }}>{props.children}</Flex>
      <AppFooter />
    </Flex>
  );
};
export { App };
